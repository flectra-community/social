# Flectra Community / social

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[mail_layout_force](mail_layout_force/) | 2.0.1.0.0| Force a mail layout on selected email templates
[mail_tracking_mailgun](mail_tracking_mailgun/) | 2.0.2.1.0| Mail tracking and Mailgun webhooks integration
[mail_activity_creator](mail_activity_creator/) | 2.0.1.0.0|         Show activities creator
[mail_attach_existing_attachment_account](mail_attach_existing_attachment_account/) | 2.0.1.0.1| Module to use attach existing attachment for account module
[mass_mailing_list_dynamic](mass_mailing_list_dynamic/) | 2.0.1.0.0| Mass mailing lists that get autopopulated
[mail_outbound_static](mail_outbound_static/) | 2.0.2.0.0| Allows you to configure the from header for a mail server.
[mail_template_multi_attachment](mail_template_multi_attachment/) | 2.0.1.0.0| Module that allows to generate multi attachments on    an email template.
[mail_activity_reply_creator](mail_activity_reply_creator/) | 2.0.1.0.1| Assign new to its creator
[mail_server_by_user](mail_server_by_user/) | 2.0.1.0.0| Email Server By User
[mail_chatter_thread_colour](mail_chatter_thread_colour/) | 2.0.1.0.0|         Allow to change the colour of threads
[mail_allow_portal_internal_note](mail_allow_portal_internal_note/) | 2.0.1.0.2| Portal users can access internal messages related to own or other companies
[mass_mailing_resend](mass_mailing_resend/) | 2.0.1.0.0| Resend mass mailings
[mail_layout_preview](mail_layout_preview/) | 2.0.1.0.1|         Preview email templates in the browser
[mail_tracking_mass_mailing](mail_tracking_mass_mailing/) | 2.0.1.1.0| Improve mass mailing email tracking
[mail_optional_follower_notification](mail_optional_follower_notification/) | 2.0.1.0.0| Choose to notify followers on mail.compose.message
[mail_full_expand](mail_full_expand/) | 2.0.1.0.0| Expand mail in a big window
[mail_optional_autofollow](mail_optional_autofollow/) | 2.0.1.0.1|         Choose if you want to automatically add new recipients as followers        on mail.compose.message
[mail_notification_with_history](mail_notification_with_history/) | 2.0.1.0.0| Add the previous chatter discussion into new email notifications.
[mass_mailing_event_registration_exclude](mass_mailing_event_registration_exclude/) | 2.0.1.0.0| Link mass mailing with event for excluding recipients
[mass_mailing_subscription_email](mass_mailing_subscription_email/) | 2.0.1.0.3| Send notification emails when contacts subscription changes.
[mail_activity_done](mail_activity_done/) | 2.0.1.0.2| Mail Activity Done
[mass_mailing_partner](mass_mailing_partner/) | 2.0.1.1.0| Link partners with mass-mailing
[mail_drop_target](mail_drop_target/) | 2.0.1.0.2| Attach emails to Odoo by dragging them from your desktop
[base_search_mail_content](base_search_mail_content/) | 2.0.1.0.0| Base Search Mail Content
[microsoft_outlook_single_tenant](microsoft_outlook_single_tenant/) | 2.0.2.0.0| Microsoft Outlook Single Tenant (DEPRECATED)
[mail_debrand](mail_debrand/) | 2.0.2.2.2| Remove Odoo branding in sent emails    Removes anchor <a href flectra.com togheder with it's parent    ( for powerd by) form all the templates    removes any 'flectra' that are in tempalte texts > 20characters    
[mail_show_follower](mail_show_follower/) | 2.0.1.1.0| Show CC document followers in mails.
[mail_tracking](mail_tracking/) | 2.0.3.1.2| Email tracking system for all mails sent
[mail_send_copy](mail_send_copy/) | 2.0.1.0.1| Send to you a copy of each mail sent by Odoo
[mail_activity_partner](mail_activity_partner/) | 2.0.1.0.0| Add Partner to Activities
[mail_notification_custom_subject](mail_notification_custom_subject/) | 2.0.1.0.0| Apply a custom subject to mail notifications
[mass_mailing_subscription_date](mass_mailing_subscription_date/) | 2.0.1.0.0| Track contact's subscription date to mailing lists
[mail_restrict_send_button](mail_restrict_send_button/) | 2.0.1.1.0| Security for Send Message Button on Chatter Area
[mail_autosubscribe](mail_autosubscribe/) | 2.0.1.0.0| Automatically subscribe partners to its company's business documents
[mail_improved_tracking_value](mail_improved_tracking_value/) | 2.0.1.0.0| Improves tracking changed values for certain type of fields.Adds a user-friendly view to consult them.
[mass_mailing_contact_partner](mass_mailing_contact_partner/) | 2.0.1.0.0| Links mailing.contacts with res.partners.
[mail_inline_css](mail_inline_css/) | 2.0.1.0.0| Convert style tags in inline style in your mails
[mail_activity_unlink_log](mail_activity_unlink_log/) | 2.0.1.0.0|         Leave a message when an activity is unlinked
[mail_activity_board](mail_activity_board/) | 2.0.1.1.0| Add Activity Boards
[mass_mailing_company_newsletter](mass_mailing_company_newsletter/) | 2.0.1.0.1| Easily manage partner's subscriptions to your main mailing list.
[mail_restrict_follower_selection](mail_restrict_follower_selection/) | 2.0.1.0.0| Define a domain from which followers can be selected
[mail_preview_audio](mail_preview_audio/) | 2.0.1.0.0|         Allow to preview audio files
[mail_preview_base](mail_preview_base/) | 2.0.1.0.0|         Base to add more previewing options
[mail_partner_opt_out](mail_partner_opt_out/) | 2.0.1.0.0| Add the partner's email to the blackmailed list
[mail_activity_team](mail_activity_team/) | 2.0.1.2.0| Add Teams to Activities
[mass_mailing_custom_unsubscribe](mass_mailing_custom_unsubscribe/) | 2.0.1.0.0| Know and track (un)subscription reasons, GDPR compliant
[mail_quoted_reply](mail_quoted_reply/) | 2.0.1.0.1|         Make a reply using a message
[mail_filter_adressee_by_contact](mail_filter_adressee_by_contact/) | 2.0.1.1.0| Adresses filter by partner contacts and users
[mass_mailing_unique](mass_mailing_unique/) | 2.0.1.0.0| Avoids duplicate mailing lists and contacts
[email_template_qweb](email_template_qweb/) | 2.0.1.0.1| Use the QWeb templating mechanism for emails
[mail_attach_existing_attachment](mail_attach_existing_attachment/) | 2.0.1.0.0| Adding attachment on the object by sending this one
[website_mass_mailing_name](website_mass_mailing_name/) | 2.0.1.0.0| Ask for name when subscribing, and create and/or link partner


