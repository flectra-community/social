# Copyright 2023 CreuBlanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Mail Activities: log on unlink",
    "summary": """
        Leave a message when an activity is unlinked""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "CreuBlanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "depends": ["mail"],
    "data": ["data/unlink_message.xml"],
}
