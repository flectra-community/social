# Copyright 2021 Open Source Integrators
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Allow Portal Users to access internal messages",
    "summary": "Portal users can access internal messages"
    " related to own or other companies",
    "version": "2.0.1.0.2",
    "category": "Social Network",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "license": "AGPL-3",
    "depends": ["mail", "portal"],
    "installable": True,
    "maintainer": ["dreispt"],
    "development_status": "Alpha",
    "data": [
        "views/res_users.xml",
        "views/assets.xml",
    ],
    "qweb": ["static/src/xml/mail_portal_template.xml"],
}
