# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Mail Preview",
    "summary": """
        Preview email templates in the browser""",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "Camptocamp SA,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "depends": ["mail"],
    "data": ["templates/email_preview.xml", "wizard/email_template_preview.xml"],
    "development_status": "Beta",
}
