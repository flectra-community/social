# Copyright 2016-2017 LasLabs Inc.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Mail Outbound Static",
    "summary": "Allows you to configure the from header for a mail server.",
    "version": "2.0.2.0.0",
    "category": "Discuss",
    "website": "https://gitlab.com/flectra-community/social",
    "author": "brain-tec AG, LasLabs, Adhoc SA, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": ["base"],
    "data": ["views/ir_mail_server_view.xml"],
}
