# Copyright 2020 Creu Blanca
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Preview audio files",
    "summary": """
        Allow to preview audio files""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Creu Blanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "depends": ["mail_preview_base"],
    "data": ["template/assets.xml"],
    "qweb": ["static/src/xml/preview.xml"],
}
