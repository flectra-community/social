# Copyright 2020 Creu Blanca
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "Mail Preview",
    "summary": """
        Base to add more previewing options""",
    "version": "2.0.1.0.0",
    "license": "LGPL-3",
    "author": "Creu Blanca,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "depends": ["mail"],
    "data": [
        "template/assets.xml",
        "views/ir_attachment_view.xml",
    ],
    "qweb": ["static/src/xml/preview.xml"],
}
