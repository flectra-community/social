# Copyright (C) 2014 - Today: GRAP (http://www.grap.coop)
# @author: Sylvain LE GAL (https://twitter.com/legalsylvain)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Mail - Send Email Copy",
    "summary": "Send to you a copy of each mail sent by Odoo",
    "version": "2.0.1.0.1",
    "category": "Social Network",
    "author": "GRAP," "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/social",
    "license": "AGPL-3",
    "depends": ["mail"],
    "installable": True,
}
