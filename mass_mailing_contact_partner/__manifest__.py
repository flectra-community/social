# Copyright 2021 Camptocamp (http://www.camptocamp.com).
# @author Iván Todorovich <ivan.todorovich@gmail.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Mass Mailing Contact Partner",
    "summary": "Links mailing.contacts with res.partners.",
    "version": "2.0.1.0.0",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "maintainers": ["ivantodorovich"],
    "website": "https://gitlab.com/flectra-community/social",
    "license": "AGPL-3",
    "category": "Marketing",
    "depends": ["mass_mailing"],
    "data": ["views/mailing_contact.xml"],
}
