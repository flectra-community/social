# Copyright 2022 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)
{
    "name": "Microsoft Outlook Single Tenant (DEPRECATED)",
    "version": "2.0.2.0.0",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Hidden",
    "website": "https://gitlab.com/flectra-community/social",
    "depends": [
        "microsoft_outlook",
    ],
    "data": ["views/res_config_settings_views.xml"],
    "installable": True,
}
